# RaionUnityECSFramework
Raion's ECS Framework inspired by Entitas (https://github.com/sschmid/Entitas-CSharp).
  
![Generic badge](https://img.shields.io/badge/release-0.02-orange.svg)
![MIT license](https://img.shields.io/badge/license-MIT-blue.svg)
