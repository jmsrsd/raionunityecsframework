using System;
using UnityEngine;

namespace raion.unityecsframework.internals {
  [Serializable]
  public struct SystemExecutorListItem {
    public bool isSystemEnable;
    public TextAsset system;
  }
}