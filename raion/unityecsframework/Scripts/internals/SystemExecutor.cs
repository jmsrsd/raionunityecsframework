using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace raion.unityecsframework.internals {
  [CreateAssetMenu(fileName = "raion.unityecsframework.SystemExecutor.asset",
                   menuName = "Raion/Unity Ecs Framework/System Executor")]
  public class SystemExecutor : ScriptableObject {
    // public static members
    public static SystemExecutor GetInstance() {
      if (_instance == null)
        if (systemExecutorList.Count > 0)
          _instance = Resources.FindObjectsOfTypeAll<SystemExecutor>()[0];

      return _instance;
    }

    // public non-static members
    public void RunThenFreeze() {
      if (_isAlreadyRunOnce)
        return;

      foreach (var system in systemModuleList)
        system.UpdateAllComponents();

      _isAlreadyRunOnce = true;
    }

    public void Unfreeze() {
      _isAlreadyRunOnce = false;
    }

    // callbacks
    private void OnEnable() {
      this.OnValidate();
    }

    private void OnValidate() {
      InitializeSystemModuleList();
    }

    // private static members
    private static SystemExecutor _instance;

    private static List<SystemExecutor> systemExecutorList {
      get {
        return new List<SystemExecutor>(Resources.FindObjectsOfTypeAll<SystemExecutor>());
      }
    }

    // serialized private non-static members
    [SerializeField]
    private
    List<SystemExecutorListItem> _systemList;

    // private non-static members
    private
    bool _isAlreadyRunOnce = false;

    private
    List<modules.SystemModule> _systemModuleList;

    private List<SystemExecutorListItem> systemList {
      get {
        if (_systemList == null)
          _systemList = new List<SystemExecutorListItem>();

        return _systemList;
      }
    }

    private
    List<string> systemModuleNameList {
      get {
        var list = new List<string>();
        foreach (var systemListItem in systemList) {
          if (systemListItem.Equals(new SystemExecutorListItem()))
            continue;

          if (systemListItem.isSystemEnable == false)
            continue;

          if (systemListItem.system == null)
            continue;

          list.Add(systemListItem.system.name);
        }

        return list;
      }
    }

    private
    List<modules.SystemModule> systemModuleList {
      get {
        if (_systemModuleList == null)
          InitializeSystemModuleList();

        return _systemModuleList;
      }
    }

    private
    void InitializeSystemModuleList() {
      _systemModuleList = new List<modules.SystemModule>();
      foreach (var systemModuleType in systemModuleTypeList)
        AddSystemModuleListItem(CreateSystemModuleOfType(systemModuleType));
    }

    private List<Type>
    systemModuleTypeList {
      get {
        var list = new List<Type>();
        foreach (var systemModuleName in systemModuleNameList) {
          Type systemModuleType =
              FindSystemModuleTypeOfQuery(systemModuleName);

          if (systemModuleType == null)
            continue;

          list.Add(systemModuleType);
        }

        return list;
      }
    }

    private
    void AddSystemModuleListItem(modules.SystemModule item) {
      if (item == null)
        return;

      _systemModuleList.Add(item);
    }

    private
    modules.SystemModule CreateSystemModuleOfType(Type systemModuleType) {
      modules.SystemModule systemModule = null;
      try {
        systemModule = (modules.SystemModule) Activator.CreateInstance(systemModuleType);
      } catch (Exception exception) {
        LogCreateSystemModuleOfTypeError(exception, systemModuleType);
      }

      return systemModule;
    }

    private
    void LogCreateSystemModuleOfTypeError(Exception exception, Type systemModuleType) {
      string systemExecutorInstanceName = ConvertColorStringToYellow(this.name + ".asset"),
             systemModuleTypeName       = ConvertColorStringToYellow(systemModuleType.Name),
             systemModuleString         = ConvertColorStringToYellow("SystemModule");

      string errorMessage =
          "In " + systemExecutorInstanceName + ":"
          + "\n" + systemModuleTypeName + " isn't " + systemModuleString + "-derived Component\n"
          + "\n"
          + "\nException message:"
          + "\n" + exception.Message
          + "\n";

      Debug.Log(errorMessage);
    }

    private
    string ConvertColorStringToYellow(string str) {
      return "<color=yellow>"
             + str
             + "</color>";
    }

    private
    Type FindSystemModuleTypeOfQuery(string query) {
      string lowerCaseQuery = query.ToLower();

      Type systemModuleType = null;
      foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies()) {
        foreach (var type in assembly.GetTypes()) {
          if (type.FullName.ToLower().Contains(lowerCaseQuery)) {
            systemModuleType = Type.GetType(type.FullName);
            break;
          }
        }
      }

      return systemModuleType;
    }
  }
}