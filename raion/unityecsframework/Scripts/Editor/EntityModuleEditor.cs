using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace raion.unityecsframework.Editor {
  [CustomEditor(typeof(modules.EntityModule))]
  public class EntityModuleEditor : UnityEditor.Editor {
    // PUBLIC NON-STATIC MEMBERS
    public override void OnInspectorGUI() {
      serializedObject.Update();

      this._list.DoLayoutList();

      serializedObject.ApplyModifiedProperties();
    }

    // UNITY'S CALLBACKS
    private void OnEnable() {
      this.OnValidate();
    }

    private void OnValidate() {
      InitializeList();
      InitializeListHeaderDrawConfiguration();
      InitializeListElementDrawConfiguration();
    }

    // PRIVATE NON-STATIC MEMBERS
    private
    ReorderableList _list;

    private void InitializeList() {
      this._list = new ReorderableList(
          serializedObject,
          serializedObject.FindProperty("_componentList"),
          true, true, true, true);
    }

    private void InitializeListHeaderDrawConfiguration() {
      this._list.drawHeaderCallback = (Rect rect) => {
        EditorGUI.LabelField(rect, "Component List");
      };
    }

    private void InitializeListElementDrawConfiguration() {
      this._list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
        var element = _list.serializedProperty.GetArrayElementAtIndex(index);

        rect.y += 2;

        EditorGUI.PropertyField(
            new Rect(
                rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight),
            element,
            GUIContent.none);
      };
    }
  }
}