using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;

namespace raion.unityecsframework.Editor {
  [CustomEditor(typeof(internals.SystemExecutor))]
  public class SystemExecutorEditor : UnityEditor.Editor {
    // PUBLIC NON-STATIC MEMBERS
    public
    override
    void OnInspectorGUI() {
      serializedObject.Update();

      this._list.DoLayoutList();

      serializedObject.ApplyModifiedProperties();
    }

    // UNITY'S CALLBACKS
    private
    void OnEnable() {
      this.OnValidate();
    }

    private
    void OnValidate() {
      InitializeList();
      InitializeListHeaderDrawConfiguration();
      InitializeListElementDrawConfiguration();
    }

    // PRIVATE NON-STATIC MEMBERS
    private
    ReorderableList _list;

    private
    void InitializeListHeaderDrawConfiguration() {
      this._list.drawHeaderCallback = (Rect rect) => {
        EditorGUI.LabelField(rect, "System List");
      };
    }

    private
    void InitializeListElementDrawConfiguration() {
      this._list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
        var element = _list.serializedProperty.GetArrayElementAtIndex(index);

        rect.y += 2;

        float checkBoxWidth = 20;

        EditorGUI.PropertyField(
            new Rect(rect.x, rect.y, checkBoxWidth, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("isSystemEnable"), GUIContent.none);

        EditorGUI.PropertyField(
            new Rect(rect.x + checkBoxWidth, rect.y, rect.width - checkBoxWidth, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("system"), GUIContent.none);
      };
    }

    private
    void InitializeList() {
      this._list = new ReorderableList(
          serializedObject,
          serializedObject.FindProperty("_systemList"),
          true, true, true, true);
    }
  }
}