﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System;

namespace raion.unityecsframework.modules {
  [CreateAssetMenu(fileName = "NewEntity.asset",
                   menuName = "Raion/Unity Ecs Framework/Entity")]
  public class EntityModule : ScriptableObject {
    // PUBLIC NON-STATIC MEMBERS
    public
    void Initialize(Entity entity) {
      InitializeComponentModuleList();
      InitializeAllComponents(entity);
    }

    public
    modules.ComponentModule FindComponentListItemOfType(Type componentType) {
      return componentModuleList.Find((item) => {
        return item.GetType().Equals(componentType);
      });
    }

    public
    int GetComponentListItemCount() {
      return componentModuleList.Count;
    }

    // UNITY'S CALLBACKS
    private
    void OnEnable() {
      this.OnValidate();
    }

    private
    void OnValidate() {
      InitializeComponentModuleList();
    }

    // PRIVATE NON-STATIC MEMBERS
    private
    void InitializeAllComponents(Entity entity) {
      foreach (var component in componentModuleList)
        component.Initialize(entity);
    }

    // serialized private non-static members
    [SerializeField]
    private
    List<TextAsset> _componentList;

    private
    List<ComponentModule> _componentModuleList;

    private
    List<TextAsset> componentList {
      get {
        if (_componentList == null)
          _componentList = new List<TextAsset>();

        return _componentList;
      }
    }

    private
    List<ComponentModule> componentModuleList {
      get {
        if (_componentModuleList == null)
          InitializeComponentModuleList();

        return _componentModuleList;
      }
    }

    private
    void InitializeComponentModuleList() {
      _componentModuleList = new List<ComponentModule>();
      foreach (var componentModuleType in componentModuleTypeList)
        AddComponentModuleListItem(CreateComponentModuleOfType(componentModuleType));
    }

    private
    List<Type> componentModuleTypeList {
      get {
        var list = new List<Type>();
        foreach (var componentModuleName in componentModuleNameList) {
          Type componentModuleType = FindComponentModuleTypeOfQuery(componentModuleName);

          if (componentModuleType == null)
            continue;

          list.Add(componentModuleType);
        }

        return list;
      }
    }

    private
    List<string> componentModuleNameList {
      get {
        var componentModuleNameList = new List<string>();
        foreach (var componentTextAsset in componentList) {
          if (componentTextAsset == null)
            continue;

          componentModuleNameList.Add(componentTextAsset.name);
        }

        return componentModuleNameList;
      }
    }

    private
    void AddComponentModuleListItem(ComponentModule item) {
      if (item == null)
        return;

      componentModuleList.Add(item);
    }

    private
    ComponentModule CreateComponentModuleOfType(Type componentModuleType) {
      ComponentModule componentModule = null;
      try {
        componentModule = (ComponentModule) Activator.CreateInstance(componentModuleType);
      } catch (Exception exception) {
        string entityModuleInstanceName = ConvertColorStringToYellow(this.name + ".asset"),
               componentModuleTypeName  = ConvertColorStringToYellow(componentModuleType.Name),
               componentModuleString    = ConvertColorStringToYellow("ComponentModule");

        string errorMessage =
            "In " + entityModuleInstanceName + ":\n"
            + componentModuleTypeName + " isn't " + componentModuleString + "-derived Component\n"
            + "\n"
            + "Exception message:\n"
            + exception.Message + "\n";

        Debug.Log(errorMessage);
      }

      return componentModule;
    }

    private
    string ConvertColorStringToYellow(string str) {
      return "<color=yellow>"
             + str
             + "</color>";
    }

    private
    Type FindComponentModuleTypeOfQuery(string query) {
      string lowerCaseQuery = query.ToLower();

      Type componentModuleType = null;
      foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies()) {
        foreach (var type in assembly.GetTypes()) {
          if (type.FullName.ToLower().Contains(lowerCaseQuery)) {
            componentModuleType = Type.GetType(type.FullName);
            break;
          }
        }
      }

      return componentModuleType;
    }
  }
}
