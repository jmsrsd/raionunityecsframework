﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace raion.unityecsframework.modules {
  public abstract class SystemModule {
    // PUBLIC NON-STATIC MEMBERS
    public
    void UpdateAllComponents() {
      foreach (var entity in GetAllEntities()) {
        UpdateSystemRelatedComponentOfEntity(entity);
      }
    }

    // PROTECTED ABSTRACT MEMBERS
    protected
    abstract
    void Update();

    protected
    abstract
    Type GetComponentTypeRelatedToSystem();

    // PROTECTED NON-STATIC MEMBERS
    protected
    ComponentType GetEntityComponentOfType<ComponentType>() where ComponentType : modules.ComponentModule {
      return (ComponentType) GetEntity().GetComponentOfType(typeof(ComponentType));
    }

    protected
    Entity GetEntity() {
      return entity;
    }

    // PRIVATE NON-STATIC MEMBERS
    private
    void UpdateSystemRelatedComponentOfEntity(Entity entity) {
      this.SetEntity(entity);

      if (IsEntitySystemRelatedComponentNull()) {
        return;
      }

      this.Update();
    }

    private
    bool IsEntitySystemRelatedComponentNull() {
      return GetEntity().GetComponentOfType(GetComponentTypeRelatedToSystem()) == null;
    }

    private
    Entity entity;

    private
    void SetEntity(Entity entity) {
      this.entity = entity;
    }

    private
    Entity[] GetAllEntities() {
      return GameObject.FindObjectsOfType<Entity>();
    }
  }
}
