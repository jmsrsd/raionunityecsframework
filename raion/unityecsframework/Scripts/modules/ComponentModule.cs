﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace raion.unityecsframework.modules {
  public interface ComponentModule {
    void Initialize(Entity entity);
  }
}
