using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace raion.unityecsframework {
  [DisallowMultipleComponent]
  public class Entity : MonoBehaviour {
    // PUBLIC NON-STATIC MEMBERS
    public
    modules.ComponentModule GetComponentOfType(Type componentType) {
      return entityModule.FindComponentListItemOfType(componentType);
    }

    // UNITY'S CALLBACKS
    private
    void OnEnable() {
      try {
        entityModule.Initialize(this);
      } catch (Exception e) {
        this.LogEntityModuleCannotBeInitializedErrorMessageOfException(e);
      }
    }

    private
    void FixedUpdate() {
      GetSystemExecutor().RunThenFreeze();
    }

    private
    void LateUpdate() {
      GetSystemExecutor().Unfreeze();
    }

    // PRIVATE NON-STATIC MEMBERS
    private
    void LogEntityModuleCannotBeInitializedErrorMessageOfException(Exception e) {
      Debug.LogError(
          string.Format(
              "{0} (of position: {1})'s Entity Module cannot be initialized\n",
              GetGameObjectName(),
              GetGameObjectPosition().ToString()));
    }

    private string GetGameObjectName() {
      return gameObject.name;
    }

    private Vector3 GetGameObjectPosition() {
      return gameObject.transform.position;
    }

    [SerializeField]
    private
    modules.EntityModule entityModule;

    private
    internals.SystemExecutor GetSystemExecutor() {
      return internals.SystemExecutor.GetInstance();
    }
  }
}
